#pragma once
#include <SFML\Graphics.hpp>
#include "SceneManager.h"
#include "BingoButton.h"
#include <cstdlib>
#include <ctime>
#include <string>


class Game
{
public:
	Game(sf::RenderWindow* window);
	void Go();


private:
	void ComposeFrame(); //draw to window
	void UpdateModel(); //update game logic


	bool isWin();



private:
	//game window
	sf::RenderWindow* window;


	BingoButton* gameButtons[9] = { nullptr };


	SceneManager sceneManager;


	BingoButton* startingButton = nullptr;

	
	std::string tileNames[9] = { "Index finger\ntyping", "Erorr\nin code", "Awkward\nstanding", "Aimless\nwandering", "Changing\nyour code", "---->", "Ultimate\nConcentration", "Questioon", "Snort" };
	bool stringTaken[9] = { false };


	sf::Text winText;
	sf::Font arial;


};

