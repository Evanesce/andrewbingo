#include "SceneManager.h"


SceneManager::scene SceneManager::GetScene()
{
	//gets the scene
	return currentScene;
}

void SceneManager::AdvanceScene()
{
	//advance the scene
	switch (currentScene)
	{
	case scene::startingScreen:
		currentScene = scene::midGame;
		break;
	case scene::midGame:
		currentScene = scene::winScreen;
		break;
	}
}


