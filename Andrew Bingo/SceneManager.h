#pragma once
#include <vector>

class SceneManager
{
public:
	enum class scene {
		startingScreen,
		midGame,
		winScreen
	};



public:
	SceneManager() = default;
	scene GetScene();

	void AdvanceScene();

	std::vector<int> usedNumbers;


private:
	scene currentScene = scene::startingScreen;
};

