#include "BingoButton.h"



BingoButton::BingoButton(sf::RenderWindow* window, SceneManager* sceneManager, BingoButton* prevButton, buttonType typeOfButton, int ID, std::string tileTitle)
	:
	window(window),
	sceneManager(sceneManager)
{
	//handle font load
	if (!arial.loadFromFile("arial.ttf"))
	{
#ifdef _DEBUG
		MessageBox(NULL, "Could not load font!", NULL, NULL);
#endif // _DEBUG
	}

	this->buttonText.setFont(arial);
	if (typeOfButton == buttonType::bingoButton)
		this->buttonText.setCharacterSize(15U);
	else
	{
		this->buttonText.setCharacterSize(30U);
	}
	this->buttonText.setFillColor(sf::Color::White);


	this->buttonRect.setSize(sf::Vector2f(99.0f, 99.0f));
	this->buttonRect.setOutlineThickness(3.0f);
	this->buttonRect.setOutlineColor(sf::Color::White);




	switch (typeOfButton)
	{
	case buttonType::startingButton:
		this->buttonText.setString("Start!");
		this->buttonRect.setPosition(sf::Vector2f(100.0f, 100.0f));
		break;
	case buttonType::bingoButton:
		this->buttonText.setString(tileTitle);
		this->positionSelect = ID;
		this->buttonRect.setPosition(locations[ID]);
		break;
	}



	this->m_rectPos = this->buttonRect.getPosition();
	//set text pos
	this->position.x = this->buttonRect.getPosition().x + this->buttonRect.getSize().x / 2 - this->buttonText.getLocalBounds().width / 2;
	this->position.y = this->buttonRect.getPosition().y + this->buttonRect.getSize().y / 2 - 15;
	this->buttonText.setPosition(this->position);
}


void BingoButton::Draw() const
{
	window->draw(this->buttonRect);
	window->draw(this->buttonText);
}

void BingoButton::Update()
{
	if (justStarted)
		sf::Mouse::setPosition(sf::Vector2i{ 0, 0 }, *window);
	justStarted = false;
	if (this->isColliding())
	{
		this->buttonRect.setFillColor(sf::Color::Red);
		hasBeenOnButton = true;
		if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left))
		{
			switch (sceneManager->GetScene())
			{
			case SceneManager::scene::startingScreen:
				sceneManager->AdvanceScene();
				break;
			case SceneManager::scene::midGame:
				this->isActive = true;
			}

		}
	}


	//not colliding
	else
	{
		//change color of just finished colliding
		if (!isActive)
		{
			srand((int)time(0));
			int colorSelect = (rand() % 3);
			this->buttonRect.setFillColor(colors[colorSelect]);
			hasBeenOnButton = false;
		}
	}
}

bool BingoButton::GetIsActive() const
{
	return this->isActive;
}

bool BingoButton::isColliding() const
{
		//check horizontally
		if (sf::Mouse::getPosition(*window).x > this->buttonRect.getPosition().x + 1 && sf::Mouse::getPosition(*window).x < this->buttonRect.getPosition().x + 99.0f)
		{
			//check vertically
			if (sf::Mouse::getPosition(*window).y > this->buttonRect.getPosition().y + 1 && sf::Mouse::getPosition(*window).y < this->buttonRect.getPosition().y + 99.0f)
				return true;
			else
				return false;
		}
		else
			return false;

}

bool BingoButton::VectorScan(int input, std::vector<int> vectorInput)
{
	bool output = false;
	for (int i = 0; i < vectorInput.size(); i++)
	{
		if (input == vectorInput[i])
			output = true;
	}
	return output;
}

