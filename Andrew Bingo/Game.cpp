#include "Game.h"



Game::Game(sf::RenderWindow* window)
	:
	window(window)
{
	//handle font load
	if (!arial.loadFromFile("arial.ttf"))
	{
#ifdef _DEBUG
		MessageBox(NULL, "Could not load font!", NULL, NULL);
#endif // _DEBUG
	}
}

void Game::Go()
{
	window->clear();

	UpdateModel();
	ComposeFrame();

	window->display();
}

void Game::ComposeFrame()
{
	switch (sceneManager.GetScene())
	{
	case SceneManager::scene::startingScreen:
		startingButton->Draw();
		break;
	case SceneManager::scene::midGame:
		for (int i = 0; i < 9; i++)
		{
			if (gameButtons[i] != nullptr)
				gameButtons[i]->Draw();
		}
		break;
	case SceneManager::scene::winScreen:
		window->draw(winText);
		break;
	}
}

void Game::UpdateModel()
{

	switch (sceneManager.GetScene())
	{
	case SceneManager::scene::startingScreen:
		if (startingButton == nullptr)
			startingButton = new BingoButton(window, &sceneManager, nullptr, BingoButton::buttonType::startingButton, NULL, "");
			startingButton->Update();
		break;
	case SceneManager::scene::midGame:
		//check for not initialized
		if (gameButtons[0] == nullptr)
		{
			//remove starting button from memory
			delete startingButton;
			startingButton = nullptr;
			//initalize buttons
			for (int i = 0; i < 9; i++)
			{
				int randInt;
				srand((int)time(NULL));
				do {
					randInt = (rand() % 9);
				} while (stringTaken[randInt]);
				stringTaken[randInt] = true;

				//first iteration
				if (i == 0)
					gameButtons[i] = new BingoButton(window, &sceneManager, nullptr, BingoButton::buttonType::bingoButton, 0, tileNames[randInt]);
				else
					gameButtons[i] = new BingoButton(window, &sceneManager, gameButtons[i-1], BingoButton::buttonType::bingoButton, i, tileNames[randInt]);
			}
		}

		//update buttons
		for (int i = 0; i < 9; i++)
		{
			gameButtons[i]->Update();
		}

		if (isWin())
		{
			sceneManager.AdvanceScene();
		}
		break;


	case SceneManager::scene::winScreen:
		winText.setCharacterSize(30U);
		winText.setFillColor(sf::Color::White);
		winText.setPosition(sf::Vector2f{150 - winText.getLocalBounds().width / 2, 150 - winText.getLocalBounds().height / 2 });
		winText.setFont(arial);
		winText.setString("BINGO!\nyou win!");
	}
}

bool Game::isWin()
{
	if (gameButtons[0]->GetIsActive() && gameButtons[1]->GetIsActive() && gameButtons[2]->GetIsActive())
		return true;
	else if (gameButtons[3]->GetIsActive() && gameButtons[4]->GetIsActive() && gameButtons[5]->GetIsActive())
		return true;
	else if (gameButtons[6]->GetIsActive() && gameButtons[7]->GetIsActive() && gameButtons[8]->GetIsActive())
		return true;
	else if (gameButtons[0]->GetIsActive() && gameButtons[3]->GetIsActive() && gameButtons[6]->GetIsActive())
		return true;
	else if (gameButtons[1]->GetIsActive() && gameButtons[4]->GetIsActive() && gameButtons[7]->GetIsActive())
		return true;
	else if (gameButtons[2]->GetIsActive() && gameButtons[5]->GetIsActive() && gameButtons[8]->GetIsActive())
		return true;
	else
		return false;
}
