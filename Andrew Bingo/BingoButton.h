#pragma once
#include <SFML\Graphics.hpp>
#include <Windows.h>
#include <cstdlib>
#include <ctime>
#include "SceneManager.h"
#include <iostream>

class BingoButton
{
public:
	enum class buttonType
	{
		startingButton,
		bingoButton
	};


public:
	BingoButton(sf::RenderWindow* window, SceneManager* sceneManager, BingoButton* prevButton, buttonType typeOfButton, int ID, std::string tileTitle);

	void Draw() const;
	void Update();

	bool GetIsActive() const;


public:
	int positionSelect;


private:
	bool isColliding() const;
	bool VectorScan(int input, std::vector<int> vectorInput);



private:
	//render window
	sf::RenderWindow* window;

	SceneManager* sceneManager;

	//rectangle for button
	sf::RectangleShape buttonRect;

	sf::Font arial;
	sf::Text buttonText;


	bool hasBeenOnButton = true;

	sf::Color colors[3] = {sf::Color::Magenta, sf::Color::Transparent, sf::Color::Blue};
	sf::Vector2f locations[9] = { sf::Vector2f{0, 0}, sf::Vector2f{ 100, 0 },sf::Vector2f{ 200, 0 },
								sf::Vector2f{ 0, 100 }, sf::Vector2f{ 100, 100 }, sf::Vector2f{ 200, 100 },
								sf::Vector2f{ 0, 200 }, sf::Vector2f{ 100, 200 }, sf::Vector2f{ 200, 200 }};


	sf::Vector2f position;


	sf::Vector2f m_rectPos;



	bool isActive = false;
	bool justStarted = true;

};

